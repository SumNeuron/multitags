const listOfFoods = [
  {"type": "fruit", "value": "apple"},
  {"type": "vegetable", "value": "asparagus"},
  {"type": "fruit", "value": "banana"},
  {"type": "drink", "value": "beer"},
  {"type": "vegetable", "value": "celery"},
  {"type": "desert", "value": "cake"},
  {"type": "fruit", "value": "dragonfruit"},
  {"type": "meat", "value": "escargo"},
  {"type": "meat", "value": "fish"},
  {"type": "fruit", "value": "grapes"}
]
