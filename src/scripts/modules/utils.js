/**
* Populates the multitag instance with tags provided in tagData
* @param {Array|Object} tagData a list or object of tags to be added. If
* tagData is an object then assumes that keys are the tag type and each value is
* a list of values under that type.
* @example
* // ["tagA", "tagB", "tagC", "tagD"]
* // or
* // {category1: ["tagA", "tagB"], category2:["tagC", "tagD"]}
* @param {multitag} multitag a multitag instance
* @param {Function} func a function for converting tag to tag data.
* if tagData is an array by default returns tag value
* if tagData is an object by default returns {key: key, value: tag}
* @return {undefined}
*/
export function populateTags(tagData, multitag, func) {

  if (Array.isArray(tagData)) {
    if (func == undefined) { func = function(tag) {return tag} }
    tagData.map(function(tag, i){
      multitag.makeTag(func(tag))
    })
  }

  else {
    if (func == undefined) { func = function(key, tag) {return {key: key, value: tag}} }
    d3.keys(tagData).map(function(key, i){
      tagData[key].map(function(tag, j) {
        multitag.makeTag(func(key, tag))
      })
    })
  }
}
