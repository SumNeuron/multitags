export function setupMultitagHtml( id, namespace ) {
  var id = !id.includes("#") ? "#"+id : id;

  var passedDiv = d3.select(id)

  var // alertsDiv,
  pFormElement,
    pInputRow,
      pInputElement,
        tagList,
          pInput,
      pButtonElement,
        pSubmitButton,
  suggestionArea

  pFormElement = (passedDiv.select('div.dsm-mt-form-element').empty()
  ? passedDiv.append('div')
  : passedDiv.select('div'))
  .attr('class', 'dsm-mt-form-element container-fluid')
  .attr('id', namespace + '-pseudo-form-element')


  pInputRow = (pFormElement.select('div.row').empty()
  ? pFormElement.append('div')
  : pFormElement.select('div'))
  .attr('class', 'row')
  .attr('id', namespace + '-pseudo-form-element-input-row')

  pInputElement = (pInputRow.select('div.dsm-mt-pseudo-input-element').empty()
  ? pInputRow.append('div')
  : pInputRow.select('div'))
  .attr('class', 'dsm-mt-pseudo-input-element col-sm-8')
  .attr('id', namespace + '-pseudo-input')

  tagList = (pInputElement.select('ul.dsm-mt-tag-area').empty()
  ? pInputElement.append('ul')
  : pInputElement.select('ul'))
  .attr('class', 'dsm-mt-tag-area soft-border')
  .attr('id', namespace + '-tag-list')

  pInput = (pInputElement.select('input.dsm-mt-typeahead-input-area').empty()
  ? pInputElement.append('input')
  : pInputElement.select('input'))
  .attr('class', 'dsm-mt-typeahead-input-area')
  .attr('id', namespace + '-typeahead-area')
  .attr('type', 'text')

  pButtonElement = (pInputRow.select('div.dsm-mt-pseudo-input-element.button-container').empty()
  ? pInputRow.append('div')
  : pInputRow.select('div'))
  .attr('class', 'dsm-mt-pseudo-input-element col-sm-4 button-container')
  .attr('id', namespace + '-pseudo-input-button-container')


  pSubmitButton = (pInputRow.select('button').empty()
  ? pInputRow.append('button')
  : pInputRow.select('button'))
  .attr('class', 'btn btn-primary btn-lg btn-block')
  .attr('id', namespace + '-pseudo-submit-button')
  .attr('type', 'button')
  .attr('name', 'button')


  suggestionArea = (pFormElement.select('div.dsm-mt-typeahead-suggestion-area').empty()
  ? pFormElement.append('div')
  : pFormElement.select('div'))
  .attr('class', 'dsm-mt-typeahead-suggestion-area')
  .attr('id', namespace + '-typeahead-suggestion-area')

  return passedDiv
}

/**
 * Generates the elements needed for an inline typeahead form
 * with the except of the form itself (makes it easier to add csrf token with templating language)
 * @param {string} id of container in which to generate the elements
 * @param {string} namespace to prepend to all elements
 * @returns {d3.selection} of the passed containing element (e.g. d3.select(id) ) after
 * all elements have been populated.
 * @example
 * // index.html
 * ...
 * <div id="my-div"></div>
 * ...
 * // script tag in index.html or js file
 * bsTag.setupInlineMultitagHtml("my-div", "mtag")
 *
 * //results in html:
 * <div id="my-div">
 *   <div class="dsm-mt-inline-form w-100" id="mtag-pseudo-form-element">
 *     <div class="dsm-mt-pseudo-input-element" id="mtag-pseudo-input">
 *       <ul class="dsm-mt-tag-area" id="mtag-tag-list">
 *         <input class="dsm-mt-typeahead-input-area" id="mtag-typeahead-area" type="text">
 *       </ul>
 *     </div>
 *     <button class="dsm-mt-btn-submit" id="mtag-pseudo-submit-button" type="button" name="button">
 *       <i class="fa fa-search" id="mtag-pseudo-submit-button-icon"></i>
 *     </button>
 *   </div>
 * </div>
 */

export function setupInlineMultitagHtml( id, namespace ) {
  var id = !id.includes("#") ? "#"+id : id;

  var passedDiv = d3.select(id)

  var // alertsDiv,
  pFormElement,
    // form
      // input
    pInputElement,
      tagList,
        pInput,
    pSubmitButton,
      pSubmitIcon,
    suggestionArea

  pFormElement = (passedDiv.select('div.dsm-mt-inline-form').empty()
  ? passedDiv.append('div')
  : passedDiv.select('div'))
  .attr('class', 'dsm-mt-inline-form w-100')
  .attr('id', namespace + '-pseudo-form-element')

    pInputElement = (pFormElement.select('div.dsm-mt-pseudo-input-element').empty()
    ? pFormElement.append('div')
    : pFormElement.select('div'))
    .attr('class', 'dsm-mt-pseudo-input-element')
    .attr('id', namespace + '-pseudo-input')

      tagList = (pInputElement.select('ul.dsm-mt-tag-area').empty()
      ? pInputElement.append('ul')
      : pInputElement.select('ul'))
      .attr('class', 'dsm-mt-tag-area')
      .attr('id', namespace + '-tag-list')

        pInput = (tagList.select('input.dsm-mt-typeahead-input-area').empty()
        ? tagList.append('input')
        : tagList.select('input'))
        .attr('class', 'dsm-mt-typeahead-input-area')
        .attr('id', namespace + '-typeahead-area')
        .attr('type', 'text')

  pSubmitButton = (pFormElement.select('button.dsm-mt-btn-submit').empty()
  ? pFormElement.append('button')
  : pFormElement.select('button'))
  .attr('class', 'dsm-mt-btn-submit')
  .attr('id', namespace + '-pseudo-submit-button')
  .attr('type', 'button')
  .attr('name', 'button')

    pSubmitIcon = (pSubmitButton.select('i').empty()
    ? pSubmitButton.append('i')
    : pSubmitButton.select('i'))
    .attr('class', 'fa fa-search')
    .attr('id', namespace + '-pseudo-submit-button-icon')


  suggestionArea = (pFormElement.select('div.dsm-mt-typeahead-suggestion-area').empty()
  ? pFormElement.append('div')
  : pFormElement.select('div'))
  .attr('class', 'dsm-mt-typeahead-suggestion-area')
  .attr('id', namespace + '-typeahead-suggestion-area')

  return passedDiv
}
