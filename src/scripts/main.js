// // Import a logger for easier debugging.
// import debug from 'debug';
// const log = debug('app:log');
//
// // The logger should only be disabled if we’re not in production.
// if (ENV !== 'production') {
//
//   // Enable the logger.
//   debug.enable('*');
//   log('Logging is enabled!');
// } else {
//   debug.disable();
// }


// Import styles (automatically injected into <head>)
import '../styles/main.css';


bsTag = {};
import {populateTags} from './modules/utils';
import multitag from './modules/multitag';
import {setupInlineMultitagHtml} from './modules/setup-multitag-html';
bsTag.multitag = multitag;
bsTag.populateTags = populateTags;
bsTag.setupInlineMultitagHtml = setupInlineMultitagHtml;
export default bsTag
