# Bootstrap Multitagging Library
 Demos and docs at [GitLab Pages][pages].

# About
This library was originally made as a multitagging library for `<textarea>`
and vertically stacked tags in bootstrap that did not seem to exist. While
this capacity exists within the library, it has stylistically diverged to be
more of an in-line tagging form. The linked demos above shows each component in
isolation.

# Dependencies
- Bootstrap (only if you want Bootstrap styling)
  - jQuery
- d3
- Bloodhound (from twitter typeahead)
- Font-Awesome 4+ (only if you want font-awesome icons)


# bsTag
The multitagging library is bundled up into the module `bsTag`. `bsTag`'s API
is influenced from d3's closures. For example to setup an instance of multitag
with a remote `Bloodhound`:

```javascript
var mtag = bsTag.multitag()
.hiddenInputArea(hiddenInputArea) // id of the actual form's input tag
.taggedList(taggedList) // id of the place where tags will be added
.typeaheadInputArea(typeaheadInputArea) // id of the input tag where the user types
.typeaheadInputSuggestionList(typeaheadInputSuggestionList) // id of the area where suggestions will appear
.pseudoInputArea(pseudoInputArea) // id of the container around the input area / tag list
.alertArea(alertArea) // id of area where errors will appear
.namespace(namespace) // to prevent selection clashes if multiple typeaheads on same page
.asyncQ(true) // if typeahead data is stored locally or not
.maxSuggestions(5) // how many suggestions to show
.bloodHound(bloodhoundInstance) // what to use for typeahead

// call mtag
mtag() // binds everything
```

## setupInlineMultitagHtml
Sets up all the necessary containers for an in-line multitag form





[pages]:  https://sumneuron.gitlab.io/multitags/
